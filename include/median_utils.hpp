/**
 * Utilites for testing the median data structures.
 */
#ifndef AZAI_MEDIAN_UTILS_HPP
#define AZAI_MEDIAN_UTILS_HPP

#include "utils.hpp"
#include "median.hpp"

namespace azai {

    class MedianUtils {
    public:
        /// Performs various tests for correctness and performance of the two implementations of median structures
        static void runTests();

        /// Checks two Median objects for equality
        static inline bool areEqual(const Median& a, const Median& b) {
            return a.valid == b.valid && fpEqual(a.value, b.value);
        }

        /// Returns the median for the provided container.
        /// Uses a trivial algorithm which sorts the input and then retrieves the median.
        static Median getMedian(ElemTypeVector& cont);
    };
}

#endif // AZAI_UTILS_HPP
