/**
 * This header provides two implementations of a data structure which
 * supports two main operations - inserting an element and getting the current median.
 * This header together with median.cpp are self-contained and can be used standalone.
 *
 * The median of a sorted sequence of values is exactly the middle positioned value
 * if the sequence has odd number of elements or the average of the two adjacent
 * middle positioned values if the sequence has even number of elements.
 *
 * One of the implementations (VectorMedian) uses a STL vector to store the values and
 * the other one (HeapsMedian) uses two STL priority_queues which are in fact heaps wrappers.
 * The difference between the two is the complexity of an element insertion which is
 * O(n) for VectorMedian and O(log(n)) for HeapsMedian. The complexity of getting the median
 * is constant O(1).
 *
 * The default choice should be HeapsMedian except for situations where a small number of
 * additions will be used (<100) and memory footprint is critical because VectorMedian offers
 * slightly better performance in that case.
 *
 * The insertion operations are not thread-safe.
 */
#ifndef AZAI_MEDIAN_HPP
#define AZAI_MEDIAN_HPP

#include <vector>
#include <algorithm>
#include <queue>
#include <limits>

namespace azai {

    /// The type of the elements that the data structures hold
    using ElemType        = int;
    using ElemTypeVector  = std::vector<ElemType>;
    /// The type of the median value - it has to be a floating point type
    using MedianValueType = float;

    /// Stores the current median state (also for convenience instead of using a std::pair)
    struct Median {
        /// Constructor - default
        Median() : value(0), valid(false) { }

        /// The value of the current median if valid
        MedianValueType value;
        /// False only for empty containers and true otherwise
        bool valid;
    };

    /// A container that provides insertion of element and getting the current median
    /// using vector as an underlying container. Could be used when little insertions will
    /// ever be made (<100) and memory footprint is critical.
    class VectorMedian {
    public:
        /// Constructor - default
        VectorMedian() : median_(), data_() {
            // reserve enough elements for the typical use case
            data_.reserve(100);
        }

        /**
         * Inserts an element to the structure with O(n).
         * @param value the element to be inserted
         * @return the current median value for convenience (always valid)
         */
        MedianValueType insert(ElemType elem);

        /**
         * Retrieve the current median.
         * It will be invalid if no insertions has taken place.
         */
        inline const Median& get() const noexcept {
            return median_;
        }

    private:
        /// Stores the current median
        Median median_;
        /// The underlying container that stores the elements in a sorted order
        ElemTypeVector data_;
    };

    /// A container that provides insertion of element and getting the current median
    /// using heaps as an underlying containers. Generally the preferred choice in almost
    /// all situations (for the exceptions see VectorMedian).
    class HeapsMedian {
    public:
        /// Constructor - default
        HeapsMedian() = default;

        /**
         * Inserts an element to the structure with O(log(n)).
         * @param value the element to be inserted
         * @return the current median value for convenience (always valid)
         */
        MedianValueType insert(ElemType elem);

        /**
         * Retrieve the current median.
         * It will be invalid if no insertions has taken place.
         */
        inline const Median& get() const noexcept {
            return median_;
        }

    private:
        /// Stores the current median
        Median median_;
        /// The two underlying heaps that store the elements in two parts.
        /// A max-heap which stores the lower half of the elements
        std::priority_queue<ElemType, ElemTypeVector, std::less<ElemType> > left_;
        /// A min-heap which stores the upper half of the elements
        std::priority_queue<ElemType, ElemTypeVector, std::greater<ElemType> > right_;
    };
}

#endif // AZAI_MEDIAN_HPP
