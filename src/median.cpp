#include "../include/median.hpp"

namespace azai {

    MedianValueType VectorMedian::insert(ElemType elem) {
        /*
         * The logic here is to keep the underlying vector sorted after each insertion.
         * Ensure this by finding the insertion position using binary_search and then inserting there.
         */
        auto pos = lower_bound(data_.begin(), data_.end(), elem);
        data_.emplace(pos, elem);

        // get the median from the sorted sequence
        const auto n = data_.size();
        median_.value = (n & 0x1)
            ? data_[n / 2]
            : MedianValueType(data_[n / 2] + data_[(n / 2) - 1]) / 2;
        median_.valid = true;

        return median_.value;
    }

    MedianValueType HeapsMedian::insert(ElemType elem) {
        /*
         * The logic here is more complicated than VectorMedian. The idea is to keep the lower and upper half of
         * the elements in two separate heaps - the lower ones (left) in max-heap and the upper ones (right) in min-heap.
         * Then for each insertion we keep a balance between the heaps so that their size difference is at most 1
         * and the median value is a function of the top() values of these 2 heaps. It is either exactly one of them
         * or it is an average of them. The insertion is made in one of the two heaps depending on the value being
         * inserted and the size the two heaps and then the median calculation is done based on analogical reasons.
         */
        if (!median_.valid) {
            // handle first insertion
            left_.emplace(elem);

            median_.value = elem;
            median_.valid = true;

            return median_.value;
        }

        if (left_.size() == right_.size()) {
            if (elem < median_.value) {
                left_.emplace(elem);
                median_.value = left_.top();
            }
            else {
                right_.emplace(elem);
                median_.value = right_.top();
            }
        }
        else if (left_.size() < right_.size()) {
            if (median_.value < elem) {
                left_.emplace(right_.top());
                right_.pop();
                right_.emplace(elem);
            }
            else {
                left_.emplace(elem);
            }
            median_.value = MedianValueType(left_.top() + right_.top()) / 2;
        }
        else { // left_.size() > right_.size()
            if (elem < median_.value) {
                right_.emplace(left_.top());
                left_.pop();
                left_.emplace(elem);
            }
            else {
                right_.emplace(elem);
            }
            median_.value = MedianValueType(left_.top() + right_.top()) / 2;
        }

        return median_.value;
    }

} // end of namespace azai
