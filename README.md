**Description**

A small project that provides two implementations of a data structure which
supports two main operations - inserting an element and getting the current median.
It also provides tests for correctness and performance of the implementations.

---

## Building and testing

**Instructions for Linux**

1. Download and extract/clone the repo in some dir - say <install_dir>
2. When in <install_dir> execute the following commands:
3.  mkdir build
4.  cd build
5.  cmake ..
6.  cmake --build .

7. Now after the azaimedian executable is built the tests can be run with
 ./azaimedian

---

## Usage

The include/median.hpp and src/median.cpp are self-contained and can be used separately.
For more information on the interface please check include/median.hpp.

