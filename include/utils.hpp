/*
 * This header provides general utilities.
 */
#ifndef AZAI_UTILS_HPP
#define AZAI_UTILS_HPP

#include <cmath>
#include <limits>
#include <chrono>

namespace azai {

    /// Checks two floating point numbers for equality
    template <typename T>
    inline bool fpEqual(T a, T b) {
        return std::abs(a - b) < std::numeric_limits<T>::epsilon();
    }

    /**
     * A utility class to benchmark execution performance with millisecond resolution.
     *
     * Simple usage (pseudocode):
     * ...
     * TimeWatch tw;
     * <code_A_to_benchmark>
     * printf("Code_A took %.3f ms\n", tw.getElapsedTimeMillis());
     * ...
     * tw.reset();
     * <code_B_to_benchmark>
     * printf("Code_B took %.3f ms\n", tw.getElapsedTimeMillis());
     * ...
     */
    class TimeWatch {
    public:
        /// Defines supported timer resolutions
        enum class Resolution : int {
            MILLISECONDS = 1000,
            MICROSECONDS = 1000000
        };

        /// Constuctor - default
        TimeWatch() : start(std::chrono::system_clock::now()) {
        }

        /// Restars the timer
        inline void reset() {
            start = std::chrono::system_clock::now();
        }

        /// Returns the elapsed time with the provided resolution
        inline double getElapsedTime(Resolution resolution) {
            std::chrono::duration<double> elapsedSeconds = std::chrono::system_clock::now() - start;
            return elapsedSeconds.count() * static_cast<std::underlying_type_t<Resolution>>(resolution);
        }

    private:
        /// Keeps the starting time point of the timer
        std::chrono::time_point<std::chrono::system_clock> start;
    };
}

#endif //AZAI_UTILS_HPP
