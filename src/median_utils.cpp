#include "../include/median_utils.hpp"

#include <cstdio>
#include <string>

using namespace std;
using namespace azai;

static const char* PASSED_STR = "PASSED";
static const char* FAILED_STR = "FAILED";

/// -------- Helper functions ----------- ///

bool evaluateTestCase(const std::string& tag, bool expression) {
    printf(" - Test %24s : %s\n", tag.c_str(), expression ? "OK" : "FAIL");
    return expression;
}

/// Tests the correctness of implementation in terms of whether VectorMedian and HeapsMedian
/// produce the same results under the same conditions.
bool testEquality() {
    bool result = true;

    printf("Starting equality test...\n");

    auto iters  = 100000;
    bool ok     = true;

    VectorMedian vectorMedian;
    HeapsMedian heapsMedian;
    // add random values and check for equality
    while (iters-- && ok) {
        const Median& vm = vectorMedian.get();
        const Median& hm = heapsMedian.get();
        ok &= MedianUtils::areEqual(vm, hm);

        auto value = rand() % 1000;
        vectorMedian.insert(value);
        heapsMedian.insert(value);
    }

    result &= evaluateTestCase("Heaps/VectorMedian consistency", ok);
    printf("Equality tests : %s\n\n", result ? PASSED_STR : FAILED_STR);

    return result;
}

/// Some helper templates for outputing the test results
template <typename MedianStruct> std::string testTag() { return "INVALID"; }
template <> std::string testTag<VectorMedian>() { return "VectorMedian"; };
template <> std::string testTag<HeapsMedian>()  { return "HeapsMedian"; };

/// Performs various correctness of implementation tests for the provided MedianStruct specialization.
/// Returns false if any of them fails or true otherwise.
template <typename MedianStruct>
bool testCorrectness() {
    bool result = true;

    const auto itersMax = 10000;
    const string& tag   = testTag<MedianStruct>();

    printf("Starting %s correctness tests...\n", tag.c_str());

    // normal cases
    {
        bool ok    = true;
        auto iters = itersMax;

        MedianStruct medianStruct;
        // add random values and check for equality of the returned value of insert() and get()
        while (iters-- && ok) {
            MedianValueType medVal     = medianStruct.insert(rand() % 100);
            const Median& median = medianStruct.get();
            ok &= (median.value == medVal);
        }
        result &= evaluateTestCase("Insert/get consistency", ok);
    }
    {
        bool ok    = true;
        auto iters = itersMax;

        MedianStruct medianStruct;
        ElemTypeVector cont;
        cont.reserve(iters);
        // add random values and check for equality with a trivial median algorithm (slow)
        while (iters-- && ok) {
            const Median& median = medianStruct.get();
            Median medianCheck = MedianUtils::getMedian(cont);
            ok &= MedianUtils::areEqual(median, medianCheck);

            MedianValueType value = rand() % 100;
            cont.emplace_back(value);
            medianStruct.insert(value);
        }
        result &= evaluateTestCase("Calculation correctness", ok);
    }
    // edge cases
    {
        MedianStruct medianStruct;
        const Median& res = medianStruct.get();
        result &= evaluateTestCase("Empty container", res.valid == false);
    }
    {
        MedianStruct medianStruct;
        const MedianValueType singleValue = 123;
        medianStruct.insert(singleValue);
        const Median& res = medianStruct.get();
        result &= evaluateTestCase("Single value container", res.valid == true && res.value == singleValue);
    }
    {
        bool ok    = true;
        auto iters = itersMax;

        MedianStruct medianStruct;
        const MedianValueType value = 123;
        while (iters-- && ok) {
            ok &= (value == medianStruct.insert(value));
        }
        result &= evaluateTestCase("Calculation same value", ok);
    }

    printf("%s correctness tests : %s\n\n", tag.c_str(), result ? PASSED_STR : FAILED_STR);

    return result;
}

/// Measures average execution performance for the provided Median structure specialization by
/// repeating the inner loop several times to get more accurate results.
template <typename MedianStruct>
double benchmarkTestCase(const ElemTypeVector& pool, const int iters, const int repeat = 10) {
    TimeWatch tw;
    double totalTime = 0;

    for (int r = 0; r < repeat; r++) {
        auto i = iters;
        MedianStruct medianStruct;

        tw.reset();
        while (i--) {
            medianStruct.insert(pool[i]);
        }
        totalTime += tw.getElapsedTime(TimeWatch::Resolution::MICROSECONDS);
    }

    totalTime /= repeat;
    const string twResSuffix = (totalTime > 1000) ? "ms" : "us";
    if (totalTime > 1000) {
        totalTime /= 1000;
    }

    printf("%s average time for %d insertions : %.3f %s\n", testTag<MedianStruct>().c_str(), iters,
            totalTime, twResSuffix.c_str());

    return totalTime;
}

/// Compares execution performance of VectorMedian vs HeapsMedian structures using the provided iterations
void comparePerformance(const ElemTypeVector& pool, const int iters) {
    double hmTime = benchmarkTestCase<HeapsMedian>(pool, iters);
    double vmTime = benchmarkTestCase<VectorMedian>(pool, iters);
    printf("Heaps/Vector Median speedup for %d insertions is %.2f%%\n\n", iters, (100 * vmTime / hmTime) - 100);
}

/// Examines execution performance of VectorMedian and HeapsMedian structures in various situations
void testPerformance() {
    printf("Starting performance tests...\n");

    // build a random sequence of values to choose from so that we remove the rand() overhead and
    // thus have maximum accurate performance measure of the insert operation
    ElemTypeVector pool(1000000);
    for (auto i = 0; i < pool.size(); i++) {
        pool[i] = rand() % 100;
    }
    random_shuffle(pool.begin(), pool.end());

    comparePerformance(pool, 100);
    comparePerformance(pool, 100000);

    printf("Finished performance tests.\n");
}

Median MedianUtils::getMedian(ElemTypeVector& cont) {
    Median result;

    if (!cont.empty()) {
        if (!is_sorted(cont.begin(), cont.end())) {
            sort(cont.begin(), cont.end());
        }

        const auto n = cont.size();
        result.value = (n & 0x1)
           ? cont[n / 2]
           : MedianValueType(cont[n / 2] + cont[(n / 2) - 1]) / 2;
        result.valid = true;
    }

    return result;
}

void MedianUtils::runTests() {
    printf("Starting median structure tests...\n\n");
    TimeWatch tw;

    bool result = true;
    result &= testEquality();
    result &= testCorrectness<VectorMedian>();
    result &= testCorrectness<HeapsMedian>();
    testPerformance();

    printf("\nMedian structure tests : %s in %.3f ms\n", result ? PASSED_STR : FAILED_STR,
            tw.getElapsedTime(TimeWatch::Resolution::MILLISECONDS));
}
